import './assets/css/imports.css';
import scriptsJs from './assets/js/scripts.js'; 


const url = 'https://blog-json.s3.eu-west-2.amazonaws.com/blogArticles.json';

let articles = [];
let indexOfPage = null;

const loadJson = () => {
    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    })
        .then(response => response.json())
        .then(articlesArray => {

            let filteredArticles = articlesArray.filter(article => {
                let showOnPage = false;
                article.whereToShow.forEach((page, index) => {
                    if (page.page.toLowerCase() === "hpus" && page.toShow === true) {
                        showOnPage = true;
                        indexOfPage = index;
                    }
                })
                return showOnPage;
            })

            articles = [...filteredArticles];
        
            createArticle(filteredArticles, indexOfPage);
    
        }).then(item => {
            scriptsJs()
           /*  setTimeout(function(){  scriptsJs(); }, 1000); */
           
        })
        .catch(error => console.log(error))
}

loadJson();


const createArticle = (array, indexOfPage) => {
    function compare(a, b) {
        if (a.whereToShow[indexOfPage].position < b.whereToShow[indexOfPage].position) {
            return -1;
        }
        if (a.whereToShow[indexOfPage].position > b.whereToShow[indexOfPage].position) {
            return 1;
        }
        return 0;
    }

    const sortedArray = [...array.sort(compare)];
    const innerSlider = document.querySelector('#slider3 .innerSlider');

    sortedArray.forEach((article, index) => {
        let blogCard = document.createElement('div');

        if (index === 0) {
            blogCard.classList.add('sliderChild');
            blogCard.classList.add('blogCard');
            blogCard.classList.add('firstChildMargin');

        } else {
            blogCard.classList.add('sliderChild');
            blogCard.classList.add('blogCard');
        }

        blogCard.innerHTML = `<a href=${article.url}> <div class="blogCard__image"><img src=${article.image}></div></a><div class="blogCard__title"><a href=${article.url}><h2>${article.title.toLowerCase()}</h2></a></div>`;
        innerSlider.appendChild(blogCard);

    })

  



}




import tabFunction from './assets/js/tabs.js';

tabFunction();
